import { TDropdown } from '../dist/vue-tableau-components.common'

export default {
  title: 'TDropdown',
  component: TDropdown,
}

const data = function () {
  return {
    cityOptions: [
      { id:1, text: "Amsterdam" },
      { id:2, text: "London" },
      { id:3, text: "Plovdiv" },
      { id:4, text: "Cape Town" },
    ],
    selectedCity: 1
  }
} 

export const Outline = () => ({
  components: { TDropdown },
  data: data,
  template: '<t-dropdown v-model="selectedCity" :options="cityOptions"></t-dropdown>'
})

export const Line = () => ({
  components: { TDropdown },
  data: data,
  template: '<t-dropdown kind="line" v-model="selectedCity" :options="cityOptions"></t-dropdown>',
})

export const Icon = () => ({
  components: { TDropdown },
  data: data,
  template: '<t-dropdown kind="icon" v-model="selectedCity" :options="cityOptions"></t-dropdown>',
})

