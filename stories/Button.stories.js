import { action } from '@storybook/addon-actions'
import { TButton } from '../dist/vue-tableau-components.common'

export default {
  title: 'TButton',
  component: TButton,
}

export const Outline = () => ({
  components: { TButton },
  template: '<t-button @click="action">Hello Button</t-button>',
  methods: { action: action('clicked') },
})

export const Primary = () => ({
  components: { TButton },
  template: '<t-button kind="primary" @click="action">Hello Button</t-button>',
  methods: { action: action('clicked') },
})

export const Destructive = () => ({
  components: { TButton },
  template: '<t-button kind="destructive" @click="action">Hello Button</t-button>',
  methods: { action: action('clicked') },
})

