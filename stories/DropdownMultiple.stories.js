import { TDropdownMultiple } from '../dist/vue-tableau-components.common'

export default {
  title: 'TDropdownMultiple',
  component: TDropdownMultiple,
}

const data = function () {
  return {
    currencyOptions: [
      { id: 1, text: 'Euro', selected: true },
      { id: 2, text: 'Lev', selected: false },
      { id: 3, text: 'USD', selected: false },
    ]
  }
} 

export const Outline = () => ({
  components: { TDropdownMultiple },
  data: data,
  template: '<t-dropdown-multiple v-model="currencyOptions"></t-dropdown-multiple>'
})

export const Line = () => ({
  components: { TDropdownMultiple },
  data: data,
  template: '<t-dropdown-multiple kind="line" v-model="currencyOptions"></t-dropdown-multiple>',
})

export const Icon = () => ({
  components: { TDropdownMultiple },
  data: data,
  template: '<t-dropdown-multiple kind="icon" v-model="currencyOptions"></t-dropdown-multiple>',
})

