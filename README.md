## Documentation for Vue Tableau Components

### To start the storybook server and see the components run 
```
npm install
npm run storybook
```

### To publish
**Note: don't forget to update the version in package.json before publishing**
```
npm run build
npm publish
```

To include a Tableau component some other component or the main vue instance, import them

```javascript
import TDropdown from "../../components/TableaUI/TDropdown.vue";

let vueExample = new Vue({
    components: {
        TDropdown
    }
})
```
---
### TButton
TButtons work the same way regular buttons do. The disabled property works the same as in a regular button Include them as a t-button element:

```HTML
<t-button kind="primary" @click="SomeFunction">Button</t-button>
<t-button kind="outline" disabled @click="SomeFunction">Disabled button</t-button>
```
##### Props
_kind:_
Represents the color of the button.
* outline (white)
* primary (green)
* destructive (red)

---

### TCheckbox
The TCheckbox wraps an input element and a label element. The disabled and checked property therefore do not work out of the box.
It is possible to have a single checkbox, as well as a group of checkboxes listening to the same variable bound to v-model.
The checked property directly listens to v-model, and can either be a normal boolean or a string.

**Single checkbox:**
```HTML
<t-checkbox v-model="selected">Checkbox</t-checkbox>
```

Multiple checkboxes require an array bound to v-model (or you could replicate the above example!). The array then gets filled with the _val_ property of the selected checkbox.
If you want the checkbox to be selected at render, pass an array with the value of the selected checkbox.

**Multiple checkboxes:**
```HTML
<t-checkbox val="1" v-model="selectedArray">Checkbox</t-checkbox>
<t-checkbox val="2" v-model="selectedArray">Checkbox</t-checkbox>
<t-checkbox val="3" v-model="selectedArray">Checkbox</t-checkbox>
```
```javascript
let vueExample = new Vue({
    data: {
        selectedArray: ["1"]
    }
})
```
The checkbox with property `val = 1` will be selected at render.

##### Props
_disabled:_
Toggles disabled state.
* true or false

### TDropdown
The TDropdown adds the Tableau styles to a regular select HTML element.

```HTML
<t-dropdown v-model="selectedCity" :options="cityOptions"></t-dropdown>
```


To disable pass the disabled property:
````HTML
<t-dropdown v-model="selectedCity" :options="cityOptions" disabled></t-dropdown>
````

Available styles: 

````HTML
<t-dropdown kind="icon" v-model="selectedCity" :options="cityOptions"></t-dropdown>
<br>
<t-dropdown kind="line" v-model="selectedCountry" :options="countryOptions"></t-dropdown> 
<br>
<t-dropdown kind="outline" v-model="selectedFruit" :options="fruitOptions"></t-dropdown> 
````

To use the component include it as usual and initiate the parent components' state in the following way:

```javascript
let vueExample = new Vue({
    data: {
        cityOptions: [
            { id:1, text: "Amsterdam" },
            { id:2, text: "London" },
            { id:3, text: "Plovdiv" },
            { id:4, text: "Cape Town" },
        ],
        selectedCity: 1
    }
})
```
, where _cityOptions_ is the array of Dropdown options and _selectedCity_ is the variable keeping the id of the currently selected element.

##### Props
_kind_
Select a different style.

Default: outline
* icon
* line
* outline

_disabled_
Toggles disabled state.
* true or false

_options_
The list of dropdown options.
* An array of objects in the following format:
```javascript
    cityOptions: [
        { id:1, text: "Amsterdam" },
        { id:2, text: "London" },
        { id:3, text: "Plovdiv" },
        { id:4, text: "Cape Town" },
    ]
```

### TDropdownMultiple
Similar to the TDropdown component, however the user can select multiple options. Note that the dropdown options are now passed in the v-model directive.

```HTML
<t-dropdown-multiple v-model="currencyOptions"></t-dropdown-multiple>
```


To disable pass the disabled property:
````HTML
<t-dropdown-multiple v-model="currencyOptions" disabled></t-dropdown-multiple>
````

Available styles: 

````HTML
<t-dropdown-multiple kind="icon" v-model="currencyOptions"></t-dropdown-multiple>
<br>
<t-dropdown-multiple kind="line" v-model="currencyOptions"></t-dropdown-multiple>
<br>
<t-dropdown-multiple kind="outline" v-model="currencyOptions"></t-dropdown-multiple>
````

To use the component include it as usual and initiate the parent components' state in the following way:

```javascript
let vueExample = new Vue({
    data: {
        currencyOptions: [
            { id: 1, text: 'Euro', selected: true },
            { id: 2, text: 'Lev', selected: false },
            { id: 3, text: 'USD', selected: false },
        ]
    }
})
```
, where _currencyOptions_ is the array of Dropdown options. For each option you should provide:
* unique id
* text of the option
* selected -> boolean indicating if the option is selected or not

##### Props
_kind_
Select a different style.

Default: outline
* icon
* line
* outline

_disabled_
Toggles disabled state.
* true or false

### TRadioGroup
Wraps the standard HTML radio input group with Tableau styles.

```HTML
<t-radio-group v-model="selectedUniversity" :options="universityOptions"></t-radio-group>
```


To disable pass the disabled property:
````HTML
<t-radio-group v-model="selectedUniversity" :options="universityOptions" disabled></t-radio-group>
````

To use the component include it as usual and initiate the parent components' state in the following way:

```javascript
let vueExample = new Vue({
    data: {
        universityOptions: [
            { id: 1, text: 'UvA' },
            { id: 2, text: 'VU' },
            { id: 3, text: 'Universiteit Utrecht' },
        ],
        selectedUniversity: 1
    }
})
```
, where _universityOptions_ is the array of Radio options. For each option you should provide:
* unique id
* text of the option

##### Props
_options_
The array of radio options

```javascript
universityOptions: [
    { id: 1, text: 'UvA' },
    { id: 2, text: 'VU' },
    { id: 3, text: 'Universiteit Utrecht' },
]
```

_disabled_
Toggles disabled state.
* true or false


### TTextField
Wraps standard HTML text input with Tableau styles. Examples for basic and more advanced use:

```HTML
<t-text-field v-model="username"></t-text-field>
<t-text-field kind="outline" v-model="username" placeholder="Enter your username" label="Username" :error-message="usernameErrorMessage"></t-text-field>
```


To disable pass the disabled property:
````HTML
<t-text-field v-model="username" disabled></t-text-field>
````

Available styles: 

````HTML
<t-text-field kind="line" v-model="username"></t-text-field>
<br>
<t-text-field kind="outline" v-model="username"></t-text-field>
<br>
<t-text-field kind="search" v-model="username"></t-text-field>
````

Formatting:
````HTML
<t-text-field v-model="username" formatting="euro"></t-text-field>
````

To use the component include it as usual and initiate the parent components' state in the following way:

```javascript
let vueExample = new Vue({
    data: {
        username: "",
        usernameErrorMessage: "Username already in use" //optional
    },
})
```

##### Props
_kind_
Select a different style.

Default: outline
* search
* line
* outline

_type_ 
Type of the HTML input element (for e.g. password, email, etc.)

_label_
Label for the input element

_placeholder_
Placeholder text to be displayed when the component is empty

_error-message_
Error message to be displayed 

_disabled_
Toggles disabled state.
* true or false

_Formatting_
Select a different formatting 
Default: no formatting
*Euro
*date
*datetime
*meter2